FROM rocker/r-ver:4.0.4
ARG BUILD_DATE=2021-06-09
WORKDIR /home/rstudio
RUN install2.r --error --skipinstalled \ 
  furrr \ 
  future \
  future.batchtools \ 
  future.apply \
  remotes \
  sessioninfo \
  haven
RUN installGithub.r \ 
  brandmaier/semtree@c926d53

